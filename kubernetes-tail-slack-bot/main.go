package main

import (
	"bufio"
	"bytes"
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"os/exec"
	"strings"

	"gopkg.in/yaml.v2"
)

type Config struct {
	MessageList []Topic `yaml:"messageList"`
}

type Topic struct {
	SearchedString string `yaml:"searchedString" json:"searchedString"`
	NotifiedUser   string `yaml:"notifiedUser" json:"notifiedUser"`
	Message        string `yaml:"message" json:"message"`
	SlackChannel   string `yaml:"slackChannel" json:"slackChannel"`
}

// Payload struct voor te bepalen welk stukje tekst meegestuurd wordt
type Payload struct {
	Text string `json:"text"`
}

func main() {
	runCmd()
}

func printToSlack() {
	cfg, err := NewConfig("conf.yml")
	if err != nil {
		log.Fatal(err)
	}
	// Hier in de payload gaan we bepalen wat er verzonden wordt naar Slack.
	for _, s := range cfg.MessageList {
		data := Payload{
			s.NotifiedUser + " " + s.Message}

		payloadBytes, err := json.Marshal(data)
		if err != nil {
			// handle err
		}

		body := bytes.NewReader(payloadBytes)

		req, err := http.NewRequest("POST", s.SlackChannel, body)
		if err != nil {
			// handle err
		}
		req.Header.Set("Content-Type", "application/json")

		resp, err := http.DefaultClient.Do(req)
		if err != nil {
			// handle err
		}
		defer resp.Body.Close()
	}
}

func runCmd() {
	// This is the command which you can also enter in the command line to see the logs. I use this command and it reads every line of text afterwards.
	cmd := exec.Command("kail", "--context", "tolling-kube-bmacc-a", "-n")
	cmdReader, err := cmd.StdoutPipe()
	if err != nil {
		log.Fatal(err)
	}

	// Here the scanner is running over every line of input.
	cfg, err := NewConfig("conf.yml")
	scanner := bufio.NewScanner(cmdReader)

	go func() {
		for scanner.Scan() {
			for _, s := range cfg.MessageList {
				if strings.Contains(scanner.Text(), s.SearchedString) {
					printToSlack()
				}
			}
		}
	}()

	if err := cmd.Start(); err != nil {
		log.Fatal(err)
	}
	if err := cmd.Wait(); err != nil {
		log.Fatal(err)
	}
}

// Reading of the yml file in which the configs are for the Slack bot
func NewConfig(file string) (conf *Config, err error) {
	data, err := ioutil.ReadFile(file)
	if err != nil {
		return
	}
	conf = &Config{}
	err = yaml.Unmarshal(data, conf)
	return
}
